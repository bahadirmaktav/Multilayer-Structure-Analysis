import numpy as np
import math

class MultilayerAnalysis:
    def __init__(self):
        pass
        #TODO Add for later. self.TemperatureEffectCal(roomTemperature=20, thermoOpticCoeff=thermoOpticCoeff, thermalExpCoeff=thermalExpCoeff, temperature=temperature)

    def layerMatrixReturnCal(self, matrixType, refractiveIndex, thickness, wavelength):
        if(matrixType == 1):
            return self.layerMatrixCal(self.dynamicalMatrixCal(refractiveIndex), self.inverseDynamicalMatrixCal(self.dynamicalMatrixCal(refractiveIndex)), self.translationalMatrixCal(thickness, wavelength, refractiveIndex))
        elif(matrixType == 2):
            return self.dynamicalMatrixCal(refractiveIndex)
        else:
            return self.inverseDynamicalMatrixCal(self.dynamicalMatrixCal(refractiveIndex))

    # ----------------------------------------- Temperature Effect Calculations ----------------------------------------
    def temperatureEffectCal(self, roomTemperature, thermalExpCoeff, thermoOpticCoeff, temperature): #TODO Only takes real of the refractive index into account.
        deltaTemperature = temperature - roomTemperature
        self.thickness = self.thickness + thermalExpCoeff * self.thickness * deltaTemperature
        self.refractiveIndex = self.refractiveIndex.real + thermoOpticCoeff * self.refractiveIndex.real * deltaTemperature
    # -------------------------------------------------------------------------------------------------------------------

    # ------------------------------------------ Matrix Calculation Functions ------------------------------------------
    def layerMatrixCal(self, dynamicalMatrix, inverseDynamicalMatrix, translationalMatrix):
        return dynamicalMatrix.dot(translationalMatrix).dot(inverseDynamicalMatrix)

    def dynamicalMatrixCal(self, refractiveIndex):
        dynamicalMatrix = np.ones((2, 2), dtype=complex)
        dynamicalMatrix[1][0] = refractiveIndex
        dynamicalMatrix[1][1] = -refractiveIndex
        return dynamicalMatrix

    def inverseDynamicalMatrixCal(self, dynamicalMatrix):
        return np.linalg.inv(dynamicalMatrix)

    def translationalMatrixCal(self, thickness, wavelength, refractiveIndex):
        translationalMatrix = np.zeros((2, 2), dtype=complex)
        phase = 2.0*math.pi*thickness*refractiveIndex/wavelength
        translationalMatrix[0][0] = complex(math.cos(phase.real)*math.exp(-1*phase.imag), math.sin(phase.real)*math.exp(-1*phase.imag))
        translationalMatrix[1][1] = complex(math.cos(phase.real)*math.exp(phase.imag), -1.0 * math.sin(phase.real)*math.exp(phase.imag))
        return translationalMatrix
    # -------------------------------------------------------------------------------------------------------------------

    # ------------------ Results Functions ------------------
    def reflectanceCal(self, totalSystemMatrix):
        return (abs(totalSystemMatrix[1][0]) / abs(totalSystemMatrix[0][0]))**2

    def transmittanceCal(self, totalSystemMatrix, lastLayerRefractiveIndex, firstLayerRefractiveIndex):
        return (lastLayerRefractiveIndex / firstLayerRefractiveIndex) * ((1 / abs(totalSystemMatrix[0][0]))**2)

    def absorptanceCal(self, totalSystemMatrix, lastLayerRefractiveIndex, firstLayerRefractiveIndex):
        reflectance = self.reflectanceCal(totalSystemMatrix)
        transmittance = self.transmittanceCal(totalSystemMatrix, lastLayerRefractiveIndex, firstLayerRefractiveIndex)
        return (1.0 - reflectance - transmittance)
    # -------------------------------------------------------