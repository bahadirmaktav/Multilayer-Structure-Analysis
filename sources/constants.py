# Layer Types
# 0 -> H -> High Index Layer Translational Matrix
# 1 -> L -> Low Index Layer Translational Matrix
# 2 -> M -> Medium Index Layer Translational Matrix
# 3 -> D -> Different Material 1 Layer Translational Matrix
# 4 -> T -> Different Material 2 Translational Matrix
# 5 -> V -> Different Material 2 Translational Matrix
# 6 -> A -> First Layer (Air) Inverse Dynamical Matrix
# 7 -> S -> Last Layer (Substrate) Dynamical Matrix

LAYER_TYPE_NUM = 8
LAYER_MATRIX_TYPE_ARR = [1, 1, 1, 1, 1, 1, 3, 2]